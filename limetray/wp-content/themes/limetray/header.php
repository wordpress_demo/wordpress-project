<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package limetray
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">	
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">  
	<?php wp_head(); ?>	
</head>
<body>

<!-- Start header -->
	<header class="limeHeader">
		<nav class="navbar navbar-expand-lg navbar-light justify-content-end">
		   <div class="container">
		   	<a class="navbar-brand" href="http://localhost/limetray/">

		   		<img src="<?php the_field('logo', 'option'); ?>" class="img-fluid invert" alt="Limetray Logo">
		   	</a>
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
		        <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
		       <ul class="navbar-nav ml-auto">
				    <li class="nav-item dropdown">
					      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Products</a>
					      <div class="dropdown-menu mega-menu">
					        <div class="row">
					        	
					        	<div class="col-sm-12">
					        		<div class="row">
					        			<div class="col-sm-4" style="border-right: 1px solid #eceff1;">
					        				<div class="item-wrap" style="
                                              margin-right: 2rem;
                                              margin-left: 2rem;
                                               " >
					        					<p style="font-weight: 600; font-size: 14px; font-family: 'Source Sans Pro', sans-serif; color: #04b261;">Discovery</p>
					        					<ul class="links">
					        						<li>
					        							<a href="http://localhost/limetray/online-food-ordering-system/">
					        					<p style="font-weight: 600; font-size: 1.1111111111111112vw; font-family: 'Source Sans Pro', sans-serif; color: #222;">Online Food Ordering System <i class="fa fa-arrow-right" style="font-size:
                                                12px!important; padding-left:6px;"></i></p>
 


					        								<p style="color: #222;font-size: 0.9111vw;font-weight: 300;font-family: 'Source Sans Pro', sans-serif;">Your restaurant’s own online ordering website & mobiile app</p>
                                                                     
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/online-food-ordering-app/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Mobile App</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/online-table-reservation-system/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Table Reservation</p>
					        								
					        							</a>
					        						</li>
					        						<li>
					        							<a href="http://localhost/limetray/website-for-restaurants/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Website Builder</p>
					        								
					        							</a>
					        						</li>


					        					</ul>
					        				</div>
					        			</div>

					        			<div class="col-sm-4" style="border-right: 1px solid #eceff1;">
					        				<div class="item-wrap" style="
                                             margin-right: 2rem;
                                             margin-left: 2rem;
                                               ">
					        					<p style="font-weight: 600; font-size: 14px; font-family: 'Source Sans Pro', sans-serif; color: #04b261;">Operations</p>
					        					<ul class="links">
					        						<li>
					        							<a href="http://localhost/limetray/restaurant-pos/">
					        								<p style="font-weight: 600; font-size: 1.1111111111111112vw; font-family: 'Source Sans Pro', sans-serif; color: #222;">Restaurant POS System <i class="fa fa-arrow-right" style="font-size: 12px!important; padding-left:6px;"></i></p>
                                                                            
					        								<p style="color: #222;font-size: 0.9111vw;font-weight: 300;font-family: 'Source Sans Pro', sans-serif;">Cloud-based POS solution with integrated online & third-party orders</p>

					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/restaurant-inventory-management/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Inventory</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/order-management-for-restaurants/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Third Party Order Management</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/kitchen-display-system/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Kitchen Display System</p>
					        								
					        							</a>
					        						</li>
					        					


					        					</ul>
					        				</div>
					        			</div>

					        			<div class="col-sm-4">
					        	<div class="item-wrap" style="
                                             margin-right: 2rem;
                                             margin-left: 2rem;">
                                              
					        					<p style="font-weight: 600; font-size: 14px; font-family: 'Source Sans Pro', sans-serif; color: #04b261;">Engagement</p>
					        					<ul class="links">
					        						<li>
					        					
					        							<a href="http://localhost/limetray/crm-solution-for-restaurants/">
					        								<p style="font-weight: 600; font-size: 1.1111111111111112vw; font-family: 'Source Sans Pro', sans-serif; color: #222;">CRM Solution <i class="fa fa-arrow-right" style="font-size: 12px!important; padding-left:6px;"></i></p>
					        								<p style="color: #222;font-size: 0.9111vw;font-weight: 300;font-family: 'Source Sans Pro', sans-serif;">Automate marketing campaigns with a fully-integrated CRM system</p>
                                                                      
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/restaurant-loyalty-management-system/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Loyalty Program</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/feedback-system-for-restaurants/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">Feedback System</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="http://localhost/limetray/ivr-service-for-restaurants/">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 400;">IVR Service</p>
					        								
					        							</a>
					        						</li>

					        					</ul>
					        				</div>
					        			</div>
					        		</div>
					        	</div>
					        </div>
					      </div>
				    </li>

				  
				     <li class="nav-item dropdown">
					      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Restaurant Type</a>
					      <div class="dropdown-menu mega-menu" style="width: 33%; right: 25rem;">
					        <div class="row" style="">
					        	<div class="col-sm-8 d-none d-sm-block" style=" border-right: 1px solid #eee;">
					        		<div class="content-wrap">
					        			<h4>Restaurant Types</h4>
					        			<p> Complete software suite customized to your restaurant needs</p>

					        			<span style="color: #04b261; font-size:15px; font-family: 'Source Sans Pro', sans-serif; font-weight: 600">
                  <a href="http://localhost/limetray/restaurant-software/">Learn More  </a><i class="fa fa-arrow-right" style="font-size:
            12px!important; padding-left:6px;"></i></span>
					        		</div>
					        	</div>
					        	<div class="col-sm-4 d-none d-sm-block">
					        		
					        					<ul class="links">
					        						<li>
					        							<a href="#">
					        								<p style="font-weight: 600; font-size: 1.1111111111111112vw; font-family: 'Source Sans Pro', sans-serif; color: #222;">Bar </p>
                                                                            
					        								

					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 600; ">Cafe</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 600; ">Cloud Kitchen</p>
					        								
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 600; ">Enterprise</p>
					        								
					        							</a>
					        						</li>


					        						<li>
					        							<a href="#">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 600; ">Dine-in</p>
					        								
					        							</a>
					        						</li>


					        						<li>
					        							<a href="#">
					        								<p style="color: #1f2532;font-size: 1.1111111111111112vw;font-family: 'Source Sans Pro', sans-serif;font-weight: 600; ">QSR</p>
					        								
					        							</a>
					        						</li>
					        					


					        					</ul>
					        	</div>
					        </div>
					      </div>
				    </li>
		            <li class="nav-item">
		                <a class="nav-link" href="http://localhost/limetray/clients/">Clients</a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Resources</a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Blog</a>
		            </li>
		              <li class="nav-item d-none d-sm-block">
		                <a class="nav-link" href="#">Career</a>
		            </li>
		            
		              <li class="nav-item demo-btn d-none d-sm-block">
		                <a class="nav-link" href="#">Free Demo</a>
		            </li>
		        </ul>

		    </div>
		   </div>
		</nav>
	</header>
	<!-- End Header -->