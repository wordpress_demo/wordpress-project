<?php
/**
 * limetray functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package limetray
 */

if ( ! function_exists( 'limetray_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function limetray_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on limetray, use a find and replace
		 * to change 'limetray' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'limetray', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		

         	/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'limetray_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'limetray_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function limetray_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'limetray_content_width', 640 );
}
add_action( 'after_setup_theme', 'limetray_content_width', 0 );

/**
 * 
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'mega_menu' => esc_html__( 'Mega menu', 'limetray' ),
		) );


// ADD MEGA MENU WALKER
require get_template_directory() . '/inc/bootstrap-navwalker.php';


// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/**
 * Enqueue styles.
 */ 
 
function limetray_styles() {
	

     wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.css', false,'1.0.0','all');

    wp_enqueue_style( 'base', get_template_directory_uri() . '/assets/css/base.css', false,'1.0.0','all');

    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css', false,'1.0.0','all');

    wp_enqueue_style( 'font-awesome.min', get_template_directory_uri() . '/assets/css/font-awesome.min.css', false,'1.0.0','all');

    wp_enqueue_style( 'owl.carousel.min', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', false,'1.0.0','all');


    wp_enqueue_style( 'owl.theme.min', get_template_directory_uri() . '/assets/css/owl.theme.min.css', false,'1.0.0','all');

    
		
}
add_action( 'wp_enqueue_scripts', 'limetray_styles' );


/**
 * Enqueue scripts 
 */

function limetray_scripts() {
	
	wp_enqueue_script( 'base', get_template_directory_uri() . '/assets/js/base.js', array(), '1.0.0', true );

    wp_enqueue_script( 'appear', get_template_directory_uri() . '/assets/js/appear.js', array(), '1.0.0', true );	

    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0.0', true  );

    
    wp_enqueue_script( 'smooth', get_template_directory_uri() . '/assets/js/smooth.js', array(), '1.0.0', true  );



    wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '1.0.0', true  );



    wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), '1.0.0', true  );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'limetray_scripts' );


add_action( 'wp_enqueue_scripts', 'limetray_scripts' );

/* Add this Footer code to functions.php file in your theme */

register_sidebar(array(
'name' => 'OPERATIONS',
'id'        => 'footer-1',
'description' => '',
'before_widget' => '<div id="footer-widget-1">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

register_sidebar(array(
'name' => 'FEATURES',
'id'        => 'footer-2',
'description' => '',
'before_widget' => '<div id="footer-widget-2">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

register_sidebar(array(
'name' => 'RESTAURANT TYPE',
'id'        => 'footer-3',
'description' => '',
'before_widget' => '<div id="footer-widget-3">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

register_sidebar(array(
'name' => 'QUICK LINKS',
'id'        => 'footer-4',
'description' => '',
'before_widget' => '<div id="footer-widget-4">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

register_sidebar(array(
'name' => 'CONTACT',
'id'        => 'footer-5',
'description' => '',
'before_widget' => '<div id="footer-widget-5">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');