

/* Start Clients logo Slider*/


$('.logo-slider').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    nav:false,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:4,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
});

$('.speaks-slider').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    nav:true,
    infinite:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})

$('.laptop-slider').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    nav:false,
    infinite:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:false
        }
    }
})
$('.clients-slider1').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    nav:false,
    infinite:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:false
        }
    }
});

/* Start Clients logo Slider*/

$('.country-sites button').click( function(){
    $('.country-sites').toggleClass('open');
});


/*on scroll fix header*/

$(window).scroll(function(){
  let sticky = $('.limeHeader'),
      scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});

/* Start Light YouTube Embeds */

    document.addEventListener("DOMContentLoaded",
        function() {
            var div, n,
                v = document.getElementsByClassName("youtube-player");
            for (n = 0; n < v.length; n++) {
                div = document.createElement("div");
                div.setAttribute("data-id", v[n].dataset.id);
                div.innerHTML = labnolThumb(v[n].dataset.id);
                div.onclick = labnolIframe;
                v[n].appendChild(div);
            }
        });

    function labnolThumb(id) {
        var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
            play = '<div class="play"></div>';
        return thumb.replace("ID", id) + play;
    }

    function labnolIframe() {
        var iframe = document.createElement("iframe");
        var embed = "https://www.youtube.com/embed/ID?autoplay=1";
        iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        this.parentNode.replaceChild(iframe, this);
    }


/* End Light YouTube Embeds */

/* Start tabs on hover*/

       $('#vid1-play li a').hover(function() {
                $('#vid1-play li').removeClass('active');
                var activevid = $(this).attr('href');
                $(this).parent().addClass('active');
                $('#vid1-play .tabcontent.active').removeClass('active');
                $('' + activevid + '').addClass('active');
       });


        $('#vid2-play li a').hover(function() {
                $('#vid2-play li').removeClass('active');
                var activevid1 = $(this).attr('href');
                $(this).parent().addClass('active');
                $('#vid2-play .tabcontent.active').removeClass('active');
                $('' + activevid1 + '').addClass('active');
       });


      



/* End tabs on hover*/

