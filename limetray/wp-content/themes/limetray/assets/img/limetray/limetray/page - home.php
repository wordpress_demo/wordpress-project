<?php
/* Template Name: Home */

 get_header(); 

  ?>
  
  <!-- Start hero -->
	<section class="heroBanner" style="background: #fff;">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="container">
						<div class="content-wrap">
							<h2>A single platform for all<br>
                             your restaurant’s needs</h2>
							<p style="font-weight: 300;
                             font-size: 1.2rem;">Grow your online business, manage your restaurant<br> operations & market your brand better with LimeTray’s<br> restaurant software suite.</p>
						</div>
						<a href="#" class="btn" style="font-weight: 600;">Free Demo</a>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="pic-wrap">
						<img src="<?php bloginfo('template_url'); ?>/assets/img/limetray-banner.png" class="img-fluid">
					</div>
				</div>	
			</div>
		</div>
	</section>
	<!-- End Hero -->

	<!-- Start Clients Logo Section -->
	<section class="clientsLogo-sec" style="background: #f7f7f7;">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 text-right">
					<div class="content-wrap">
						<h2>Trusted By 4500+ Restaurants</h2>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="pic-wrap">
						<div class="pic">
							<img src="<?php bloginfo('template_url'); ?>/assets/img/burger-king-limetray.png" class="img-fluid">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/krispy-kreme-limetray.png">
						</div>
					</div>
					
					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/limetray-client-sagarratna.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/russos-logo.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/biryani-blues-limetray.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/rollsmanialogo.png">
						</div>
					</div>	

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/mod-limetray.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/bikanervalalogoweb.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/dana-choga-limetray.png">
						</div>
					</div>

					<div class="pic-wrap">
						<div class="pic">
							<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/california-pizza-limetray.png">
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<!-- End Clients Logo Section -->
	<!-- Start LimeTray Product Section -->
<section class="product-sec">
<div class="container">
	<div class="sec-head">
				<h2>End-to-end marketing & technology<br>
solutions for restaurants</h2>
			</div>
	<div class="row">
		<div class="container">
	<div class="row">
	    <div class="col-md-4">
    		<div class="card mb">
              <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/img/online-1.png" alt="Card image cap">
              <div class="card-body mb">
                <h5 class="card-title" style="text-align: center;"><b>Online Food Ordering System</b><i class="fa fa-arrow-right" style="color:#000; margin-left: 7px;
                margin-top: 6px;
               font-size: 14px !important; "></i></h5>
                <p class="card-text" style="text-align: center;">Your restaurant’s own online ordering website & app</p>
                <p style="text-align: center;"><button type="button" class="btn btn-primary mb" >LEARN MORE</button></p>
              </div>
            </div>
        </div>
        <div class="col-md-4">
    		<div class="card mb">
              <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/img/POS-2.png" alt="Card image cap">
              <div class="card-body mb">
                <h5 class="card-title" style="text-align: center;"><b>Restaurant POS System</b><i class="fa fa-arrow-right" style="color:#000; margin-left: 7px;
                 margin-top: 6px;
                 font-size: 14px !important; "></i></h5>
                <p class="card-text" style="text-align: center;">Cloud-based POS solution with integrated online & third-party orders</p>
                <p style="text-align: center;"><button type="button" class="btn btn-primary mb">LEARN MORE</button></p>
              </div>
            </div>
        </div>
        <div class="col-md-4">
    		<div class="card mb">
              <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/img/CRM-1.png" alt="Card image cap">
              <div class="card-body mb">
                <h5 class="card-title" style="text-align: center;"><b>CRM Solution</b><i class="fa fa-arrow-right" style="color:#000; margin-left: 7px;
    margin-top: 6px;
    font-size: 14px !important; "></i></h5>
                <p class="card-text" style="text-align: center;">Automate marketing campaigns with a fully-integrated CRM system</p>
               <p style="text-align: center;"> <button type="button" class="btn btn-primary mb">LEARN MORE</button></p>
              </div>
            </div>
        </div>
	</div>
</div>
	</div>
</div>
</section>


	<!--  End Product Section -->

	<!-- Start Rating Section -->
<section class="rating-sec" style="background: #f7f7f7;">
              <div class="container">
               <div class="sec-head">
               	 <h2>Favourably Rated On Software Review Sites</h2>
               </div>
              	<div class="row">
              		  <div class="col-sm-12">
		                  <div class="pic-wrap">
		                    <div class="logo">
		                        <a href="https://www.softwaresuggest.com/limetray-point" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/usa/s-suggest-blue-logo.png">
		                            </a>
		                        </div>
		                    <div class="rating">
		                        <img src="<?php bloginfo('template_url'); ?>/assets/img/usa/rating-icon.png">
		                    </div> 
		                   </div>
		                   
		                   <div class="pic-wrap">
		                    <div class="logo">
		                        <a href="https://www.capterra.com/p/161232/POINT-by-LimeTray/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/usa/capterra-software-logo.png"></a></div> 
		                    <div class="rating">
		                        <img src="<?php bloginfo('template_url'); ?>/assets/img/usa/rating-icon.png">
		                    </div>    
		                   </div> 

		                    <div class="pic-wrap">
		                    <div class="logo">
		                        <a href="https://www.softwareadvice.com/retail/limetray-profile/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/usa/software-advice-logo.svg">
		                        </a>
		                        </div>    
		                    <div class="rating">
		                        <img src="<?php bloginfo('template_url'); ?>/assets/img/usa/rating-icon.png">
		                    </div>  
		                   </div> 
                  
                </div>
              	</div>
              </div>
</section>	




    <!-- testimonials starts -->
<section class="pt-5" id="testimonial-section" style="background-color: rgb(35, 50, 57); padding-top: 0rem!important;">
	<div class="clearfix pb-3"></div>
	<div id="testimonials" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ul class="carousel-indicators">
		<li data-target="#testimonials" data-slide-to="0" class="active"></li>
		<li data-target="#testimonials" data-slide-to="1"></li>
		<li data-target="#testimonials" data-slide-to="2"></li>
	</ul>

	<!-- The slideshow -->
	<div class="carousel-inner">
		<div class="carousel-item active">

			<div class="testi-text-wrap">
				<h3 class="testi-text" style="line-height: 1.2;
    font-size: 23px;
    font-family: 'Source Sans Pro', sans-serif;
    color: #fff;
    font-weight: 300;
    font-style: italic;">"I have tried 46 POS systems before settling down with LimeTray's POS. They solve massive problems for the restaurant industry and are the best technology partner you can get."</h3>
			</div>	
			<div class="testi-footer">
				<div class="testi-ftr-wrap">
				
				<img src="<?php bloginfo('template_url'); ?>/assets/img/11-16.png" class="rounded custimg" alt="user image" />               
					<div class="cust-pro-info" style="font-size: 20px;
    font-family: 'Source Sans Pro', sans-serif;"><span class="cust_name">Nikhil Hedge</span>
					<span class="cust_pro" style="font-size: 18px;
    font-family: 'Source Sans Pro', sans-serif;">Founder & Owner , Smally's</span>              	
				</div>

					
				</div>
			</div>
		</div>


		<div class="carousel-item">
			<div class="testi-text-wrap">
				<h3 class="testi-text" style="line-height: 1.2;
    font-size: 23px;
    font-family: 'Source Sans Pro', sans-serif;
    color: #fff;
    font-weight: 300;
    font-style: italic;">"It was very important for us to integrate the website, which is the centerpiece of our activity to all the other marketing activities that we are doing. That helps us understand how to acquire better."</h3>
			</div>	
			<div class="testi-footer">
				<div class="testi-ftr-wrap">
					<img src="<?php bloginfo('template_url'); ?>/assets/img/4-09.png" class="rounded custimg" alt="user image" />
					<div class="cust-pro-info" style="font-size: 20px;
    font-family: 'Source Sans Pro', sans-serif;"><span class="cust_name">Ankush Dadu
</span>
					<span class="cust_pro" style="font-size: 20px;
    font-family: 'Source Sans Pro', sans-serif;">@Owner, Anand Sweets</span></div>
				</div>
			</div>
		</div>


		<div class="carousel-item">
			<div class="testi-text-wrap">
				<h3 class="testi-text" style="line-height: 1.2;
    font-size: 23px;
    font-family: 'Source Sans Pro', sans-serif;
    color: #fff;
    font-weight: 300;
    font-style: italic;">"I have tried 46 POS systems before settling down with LimeTray's POS. They solve massive problems for the restaurant industry and are the best technology partner you can get."</h3>
			</div>	
			<div class="testi-footer">
				<div class="testi-ftr-wrap">
					<img src="<?php bloginfo('template_url'); ?>/assets/img/1-09.png" class="rounded custimg" alt="user image" />
					<div class="cust-pro-info" style="font-size: 20px;
    font-family: 'Source Sans Pro', sans-serif;"><span class="cust_name">Kabir Jeet Singh</span>
					<span class="cust_pro" style="font-size: 20px;
    font-family: 'Source Sans Pro', sans-serif;">Founder, Burger Singh</span></div>
				</div>
			</div>
		</div>


	</div>
	</div>
</section>	


	<!--End case study section -->

	<!-- Start LimeTray Advantage Section -->
<section class="product-sec">
     <div class="container">
	 <div class="sec-head">
               	 <h2>The LimeTray Advantage</h2>
     </div>
	<div class="row">

		<div class="col-sm-6">
          <div class="media">
            <a class="media-left media-middle" href="#">
            	<object data="<?php bloginfo('template_url'); ?>/assets/img/Customer Support1.svg" type="image/svg+xml" style="margin-right: 2.5rem;"></object>
           
            </a>
            <div class="media-body">
              <h4 class="media-heading" style="color: #222; font-size: 1.6666666666666665vw; font-weight: 600; padding: 2vh 0vh; font-family: 'Source Sans Pro', sans-serif;">Customer Support</h4>
             <p style="font-size: 18px;font-family: 'Source Sans Pro', sans-serif; color: #222;font-weight: 300;"> Our support team is available on call & live chat 7 days a week, throughout the year.</p>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="media">
            <a class="media-left media-middle" href="#">
            <object data="<?php bloginfo('template_url'); ?>/assets/img/Account Manager.svg" type="image/svg+xml" style="margin-right: 2.5rem;"></object>
            </a>
            <div class="media-body">
              <h4 class="media-heading" style="color: #222; font-size: 1.6666666666666665vw; font-weight: 600; padding: 2vh 0vh; font-family: 'Source Sans Pro', sans-serif;">Dedicated Account Manager</h4>
             <p style="font-size: 18px;font-family: 'Source Sans Pro', sans-serif; color: #222;font-weight: 300;"> You will get a dedicated account manager to help you with everything related to our products & services.</p>
            </div>
          </div>
        </div>

	</div>

	<div class="row">

		<div class="col-sm-6">
          <div class="media">
            <a class="media-left media-middle" href="#">
           <object data="<?php bloginfo('template_url'); ?>/assets/img/Platform.svg" type="image/svg+xml" style="margin-right: 2.5rem;"></object>
            </a>
            <div class="media-body">
              <h4 class="media-heading" style="color: #222; font-size: 1.6666666666666665vw; font-weight: 600; padding: 2vh 0vh; font-family: 'Source Sans Pro', sans-serif;">Platform</h4>
             <p style="font-size: 18px;font-family: 'Source Sans Pro', sans-serif; color: #222;font-weight: 300;"> All LimeTray products talk to each other which means you get visibility of your entire business on a single platform.</p>

              <p style="padding-top: 15px;"> <a href="#"><span style="color:#04b261; text-decoration:underline;"><b>See all products</b></span></a></p>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="media">
            <a class="media-left media-middle" href="#">
            <object data="<?php bloginfo('template_url'); ?>/assets/img/Integration.svg" type="image/svg+xml" style="margin-right: 1rem;"></object>
            </a>
            <div class="media-body">
               <h4 class="media-heading" style="color: #222; font-size: 1.6666666666666665vw; font-weight: 600; padding: 2vh 0vh; font-family: 'Source Sans Pro', sans-serif;">Integrations</h4>
             <p style="font-size: 18px;font-family: 'Source Sans Pro', sans-serif; color: #222;font-weight: 300;">Manage all essential integrations - third-parties, POS, online payments & more - on LimeTray’s platform.</p>
             <p style="padding-top: 15px;"><a href="#"><span style="color:#04b261; text-decoration:underline; font-weight: 500">See Integrations</span></a></p>
            </div>
          </div>
        </div>

	</div>
</div>
</section>


	<!-- Start Form Section -->
	<section class="form-sec" style="background: #f7f7f7;">
		<div class="container">
			<div class="sec-head">
				<h2>Get <span>On-Lime!</span></h2>
				<p>Request a free demonstration to see how our products 
				can help you boost your business</p>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-wrap">
						<form>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
								    	<input type="text" class="form-control"  placeholder="Your Name">
								  </div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
								    	 <select class="form-control">
									      <option>Select Country</option>
									      <option>2</option>
									      <option>3</option>
									      <option>4</option>
									      <option>5</option>
									    </select>
								  </div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
								    	<input type="text" class="form-control" placeholder="Your City">
								  </div>
								</div>

								<div class="col-sm-12">
									<div class="form-group">
								    	<input type="text" class="form-control" placeholder="Mobile or Landline with (STD)">
								  </div>
								</div>

								<div class="col-sm-12">
									<div class="form-group">
								    	<input type="text" class="form-control" placeholder="Restaurant Name">
								  </div>
								</div>

								<div class="col-sm-12">
									<div class="form-group">
								    	<input type="text" class="form-control" placeholder="Email Id">
								  </div>
								</div>


							</div>
						  <button type="submit" class="btn btn-block">Submit</button>
						</form>
						<h5>For any queries, contact our support team. 
+91 80 101 30055 / <a href="mailto:contactus@limetray.com" target="_top">contactus@limetray.com</a></h5>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Start Form Section -->
		<!-- Start Limetray Footer -->

	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="item-wrap text-left">
						<h4>QUICK LINKS</h4>
						<ul>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Clients</a></li>
							<li><a href="#">Pulse</a></li>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">News & Events</a></li>
							<li><a href="#">Terms</a></li>
							<li><a href="#">Support</a></li>
						</ul>
					</div>
				</div>

				<div class="col-sm-4 text-center">
					<div class="item-wrap">
						<h4>KEEP IN TOUCH</h4>
						
						<div class="foot-social">
							 <a href="https://www.facebook.com/GetOnlime" target="_blank" >
				              <i class="fa fa-facebook" aria-hidden="true"></i>
				            </a>
				             <a href="https://twitter.com/getonlime" target="_blank" >
				              <i class="fa fa-twitter" aria-hidden="true"></i>
				            </a>
				             <a href="https://www.instagram.com/limetray/" target="_blank" >
				              <i class="fa fa-instagram" aria-hidden="true"></i>
				            </a>
				             <a href="https://www.linkedin.com/company/limetray" target="_blank" >
				              <i class="fa fa-linkedin" aria-hidden="true"></i>
				            </a>
						</div>

					</div>
				</div>

				<div class="col-sm-4 text-right">
					<div class="item-wrap">
						<h4>CONTACT INFO</h4>

						<address>
							<a href="https://goo.gl/maps/hfdk3W2W7yx">
							1st floor, 445,<br>
							Udyog Vihar Phase V,<br>
							Gurugram, Haryana 122008, India<br>
							</a>
						</address>
						<p><a class="email" href="mailto:contactus@limetray.com?subject=">contactus@limetray.com</a></p>
						<p><a class="mobile" href="tel:+91 80 101 30055">+91 80 101 30055</a>></p>

						<div class="country-sites">
					         <button class="country-btn">
					             <i class="fa fa-globe" aria-hidden="true"></i>
					             <span>India</span>
					         </button>
					         <div class="dropdown">
					             <a href="https://limetray.com/">India</a>
					             <a href="https://limetray.com/usa">USA</a>
					         </div>
				      </div>
					</div>
				</div>

				<div class="col-sm-12 limetray-copyright">
		            <div class="text-center">
		            <p>Copyright © 2019 LimeTray. All Rights Reserved</p>
		            </div>
				</div>

			</div>

		</div>
	</footer>

	<!-- End Limetray Footer -->
