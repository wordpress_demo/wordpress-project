<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package limetray
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/style.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/appear.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/base.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
	 <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php wp_head(); ?>
	<style>
	html {
    margin-top: 0px !important;
         }
	</style
</head>

<body>
	<!-- Start header -->
	<header class="limeHeader">
		<nav class="navbar navbar-expand-sm navbar-light justify-content-end">
		   <div class="container">
		   	<a class="navbar-brand" href="#">
		   		<img src="<?php bloginfo('template_url'); ?>/assets/img/limetray-logo.png" class="img-fluid invert" alt="Limetray Logo">
		   	</a>
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
		        <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
		        <ul class="navbar-nav ml-auto">
				    <li class="nav-item dropdown">
					      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Products</a>
					      <div class="dropdown-menu mega-menu">
					        <div class="row">
					        	<div class="col-sm-4 d-none d-sm-block">
					        		<div class="content-wrap">
					        			<h4>LimeTray Suite</h4>
					        			<p> A complete solution for your restaurant’s technology, operations and marketing needs. All within an integrated framework.</p>
					        		</div>
					        	</div>
					        	<div class="col-sm-8">
					        		<div class="row">
					        			<div class="col-sm-4">
					        				<div class="item-wrap">
					        					<h4>Discovery</h4>
					        					<ul class="links">
					        						<li>
					        							<a href="#">
					        								<h5>Online Food Ordering System</h5>
					        								<h6>Your own online food ordering system</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>Mobile App</h5>
					        								<h6>Your branded Android & iOS app</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>Table Reservation</h5>
					        								<h6>Restaurant Reservation System</h6>
					        							</a>
					        						</li>
					        						<li>
					        							<a href="#">
					        								<h5>Website-builder</h5>
					        								<h6>Build your own restaurant website</h6>
					        							</a>
					        						</li>


					        					</ul>
					        				</div>
					        			</div>
					        			<div class="col-sm-4">
					        				<div class="item-wrap">
					        					<h4>Operations</h4>
					        					<ul class="links">
					        						<li>
					        							<a href="#">
					        								<h5>Restaurant POS</h5>
					        								<h6>Cloud based Point-of-Sale solution</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>Order Management</h5>
					        								<h6>Manage your orders from all channels</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>IVR Service</h5>
					        								<h6>Cloud Telephony Solution</h6>
					        							</a>
					        						</li>
					        					


					        					</ul>
					        				</div>
					        			</div>

					        			<div class="col-sm-4">
					        				<div class="item-wrap">
					        					<h4>Engagement</h4>
					        					<ul class="links">
					        						<li>
					        							<a href="#">
					        								<h5>CRM Solution</h5>
					        								<h6>Your SMS & Email Campaign Manager</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>Loyalty Program</h5>
					        								<h6>Complete Restaurant Loyalty Management</h6>
					        							</a>
					        						</li>

					        						<li>
					        							<a href="#">
					        								<h5>Feedback System</h5>
					        								<h6>Advanced Feedback Management Tool</h6>
					        							</a>
					        						</li>

					        					</ul>
					        				</div>
					        			</div>
					        		</div>
					        	</div>
					        </div>
					      </div>
				    </li>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Clients</a>
		            </li>
		              <li class="nav-item">
		                <a class="nav-link" href="#">Blog</a>
		            </li>
		              <li class="nav-item d-none d-sm-block">
		                <a class="nav-link" href="#">Career</a>
		            </li>
		            
		              <li class="nav-item demo-btn d-none d-sm-block">
		                <a class="nav-link" href="#">Free Demo</a>
		            </li>
		        </ul>
		        <ul class="navbar-nav d-block d-sm-none">
		        	 <li class="nav-item">
		                <a class="nav-link" href="#">About Us</a>
		             </li>
		             <li class="nav-item">
		                <a class="nav-link" href="#">Career</a>
		            </li>
		             <li class="nav-item">
		                <a class="nav-link" href="#">Pulse</a>
		            </li>
		            <div class="divider"></div>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Terms</a>
		            </li>
		             <li class="nav-item">
		                <a class="nav-link" href="#">Privacy Policy</a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Support</a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="#">Contact Us</a>
		            </li>

		            <div class="social">
		            	 <a href="https://www.facebook.com/GetOnlime" target="_blank" >
			              <i class="fa fa-facebook" aria-hidden="true"></i>
			            </a>
			             <a href="https://twitter.com/getonlime" target="_blank" >
			              <i class="fa fa-twitter" aria-hidden="true"></i>
			            </a>
			             <a href="https://www.instagram.com/limetray/" target="_blank" >
			              <i class="fa fa-instagram" aria-hidden="true"></i>
			            </a>
			             <a href="https://www.linkedin.com/company/limetray" target="_blank" >
			              <i class="fa fa-linkedin" aria-hidden="true"></i>
			            </a>
		            </div>

		            <li class="nav-item demo-btn">
		                <a class="nav-link" href="#">Free Demo</a>
		            </li>

		        </ul>

		    </div>
		   </div>
		</nav>
	</header>
	<!-- End Header -->