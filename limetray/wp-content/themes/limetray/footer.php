<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package limetray
 */

?>

	</div><!-- #content -->

		<!-- Start Limetray Footer -->

	<footer class="site-footer d-none d-lg-block">
		<div class="container">
			<div class="row">

				<div class="col-sm-3">
					<div class="item-wrap text-left">

					<div id="footer-widget-1">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
                    <?php endif; ?>
                    </div>						
					</div>								
				</div>



				<div class="col-sm-2 text-left">
					<div class="item-wrap">
					<div id="footer-widget-2">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
                    <?php endif; ?>
                    </div>						
				</div>
			    </div>


				<div class="col-sm-2 text-left" style="position: relative;
                right: auto;
                left: 3rem;">
					<div class="item-wrap">
						<div id="footer-widget-3">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
                    <?php endif; ?>
                    </div>

					</div>
				</div>

				<div class="col-sm-2 text-left new-1" style="position: relative;
                right: auto;
                left: 5rem;">
					<div class="item-wrap">
						<div id="footer-widget-4">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-4') ) : ?>
                    <?php endif; ?>
                    </div>

					</div>
				</div>


				<div class="col-sm-3 text-right">
					<div class="item-wrap">
						
                <div id="footer-widget-5">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-5') ) : ?>
                    <?php endif; ?>
                    </div>
						
					</div>
				</div>


				

				<div class="col-sm-12 limetray-copyright">
		            <div class="text-center">
		            <p>Copyright © 2019 LimeTray. All Rights Reserved</p>
		            </div>
				</div>

			</div>

		</div>
		
	</footer>

	<!-- End Limetray Footer -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
