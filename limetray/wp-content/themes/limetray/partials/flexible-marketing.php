<!-- End-To-End Marketing & Technology
Solutions For Restaurants -->
<section class="product-sec" style="background-color: <?php the_sub_field('background_color'); ?>;">
<div class="container">
	<div class="sec-head">
				<h3 style="font-weight: 600; font-size: 2.361111111111111vw;
    line-height: 1.2 !important;font-family: 'Source Sans Pro', sans-serif;color: #222;">End-to-end marketing & technology<br>
solutions for restaurants</h3>
			</div>
	<div class="row">
		<div class="container">
	<div class="row">
<?php
  // Name of Repeater field
  $names_array = get_sub_field( 'marketing' );
  
  // Counts Users
  $name_count = count( $names_array );
  
  // Number of columns total you want
  $colcounter = 3;
  
  // Determines the number of users per column
  $names_per_list = $name_count / 3 ;
  
  // Rounds decimals up to next number
  $list_names = ceil( $names_per_list );
  
  //splits Array into 3 smaller arrays
  $name_chunks = array_chunk( $names_array, $list_names );
?>       
       
      <?php foreach ( $name_chunks as $name_chunk ) : ?>
      
<?php foreach ( $name_chunk as $names ) : ?>
	    <div class="col-md-4">
    		<div class="card mb">

<?php if($names['image'] ): ?>
        <img src="<?php echo esc_html ($names['image']); ?>" class="card-img-top"  alt="Card image cap"/>
      <?php endif; ?>            
              <div class="card-body mb">
               <a href="<?php if($names['text_link'] ): ?>
        <?php echo esc_html ($names['text_link']); ?>
      <?php endif; ?>"> <h5 class="card-title" style="text-align: center; color: #04b261; "><b><?php echo esc_html( $names['heading'] );  ?></b><i class="fa fa-arrow-right" style="color:#000; margin-left: 7px;
                margin-top: 6px;
               font-size: 14px !important; "></i></h5></a>
                <p class="card-text" style="text-align: center;font-size: 18px;
    line-height: 1.75vw !important;"><?php echo esc_html( $names['content'] );  ?></p>
                <p style="text-align: center;"><a href="<?php if($names['link'] ): ?>
        <?php echo esc_html ($names['link']); ?>
      <?php endif; ?>"><button type="button" class="btn btn-primary mb" style="background: #04b261;" ><?php echo esc_html( $names['text'] );  ?></button></a></p>
              </div>
            </div>
        </div>
<?php endforeach; ?>       
<?php endforeach; ?>

	</div>
</div>
	</div>
</div>
</section>
	<!--  End----------------- -->


  <!--End case study section -->
<section class="form-sec" style="background-color: rgb(35, 50, 57); padding: 30px 0">
  <div class="container">
  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide sq-crousal4" data-ride="carousel" id="sq-crousal4">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#sq-crousal4" data-slide-to="0" class="active"></li>
          <li data-target="#sq-crousal4" data-slide-to="1"></li>
          <li data-target="#sq-crousal4" data-slide-to="2"></li>
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
        




          <!-- Quote 1 -->
          <div class="carousel-item active">
            
              <div class="row">
                <div class="col-md-12 d-md-flex d-block text-center text-lg-left">
          <img class="" src="<?php bloginfo('template_url'); ?>/assets/img/11-16.png" style="width: 450px;
    height: auto;">
          <div class="feedback-text pl-3">
            <h3 style="color: #fff;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 1.3rem;
    font-style: italic;
    padding-top: 100px;
    margin: auto;
    text-align: center;
    padding-bottom: 20px;
    ">"I have tried 46 POS systems before settling down with LimeTray's POS. They solve massive problems for the restaurant industry and are the best technology partner you can get."</h3>
      <div style="color: #fff; font-size: 20px; font-weight:800; font-family: 'Source Sans Pro', sans-serif; text-align: center;">Nikhil Hedge</div>
      <div style="color: #fff; font-size: 20px; text-align: center;">Founder & Owner , Smally's</div>                                                        
          </div>
          </div>
          </div>           
          </div>



          <!-- Quote 2 -->
          <div class="carousel-item">       
           <div class="row">
                <div class="col-md-12 d-md-flex d-block text-center text-lg-left">
          <img class="" src="<?php bloginfo('template_url'); ?>/assets/img/4-09.png" style="width: 450px;
    height: auto;">
          <div class="feedback-text pl-3">
            <h3 style="color: #fff;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 1.3rem;
    font-style: italic;
    padding-top: 100px;
    margin: auto;
    text-align: center;
    padding-bottom: 20px;
    ">"It was very important for us to integrate the website, which is the centerpiece of our activity to all the other marketing activities that we are doing. That helps us understand how to acquire better."</h3>
            <div style="color: #fff; font-size: 20px; font-weight:800; font-family: 'Source Sans Pro', sans-serif; text-align: center; ">Ankush Dadu</div>
                        <div style="color: #fff; font-size: 20px; text-align: center;">Owner, Anand Sweets</div>
            
          </div>
          </div>
          </div>        
          </div>



          <!-- Quote 3 -->
          <div class="carousel-item">
           
         <div class="row">
                <div class="col-md-12 d-md-flex d-block text-center text-lg-left">
          <img class="" src="<?php bloginfo('template_url'); ?>/assets/img/1-09.png" style="width: 450px;
    height: auto;">
          <div class="feedback-text pl-3">
            <h3 style="color: #fff;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 1.3rem;
    font-style: italic;
    padding-top: 100px;
    margin: auto;
    text-align: center;
    padding-bottom: 20px;
    ">"I have tried 46 POS systems before settling down with LimeTray's POS. They solve massive problems for the restaurant industry and are the best technology partner you can get."</h3>
            <div style="color: #fff; font-size: 20px; font-weight:800; font-family: 'Source Sans Pro', sans-serif; text-align: center; ">Kabir Jeet Singh</div>
                        <div style="color: #fff; font-size: 20px;  text-align: center;">Founder, Burger Singh</div>
            
                </div>
                </div>
                </div>
           
          </div>
        </div>
        
        <!-- Carousel Buttons Next/Prev -->
         <!-- Left and right controls -->
   <!--   <a class="carousel-control-prev" href="#sq-crousal4" data-slide="prev">
      <span class="fa fa-angle-left"></span>
      </a>
      <a class="carousel-control-next" href="#sq-crousal4" data-slide="next">
      <span class="fa fa-angle-right"></span> -->
      </a>
      </div>                          
    </div>
  </div>
</div>
</section>
