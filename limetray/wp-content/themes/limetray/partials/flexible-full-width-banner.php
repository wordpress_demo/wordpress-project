
<?php
$full_width_banner = get_sub_field('full_width_banner');
if( $full_width_banner ): ?>
<!-- End Header -->
<header class="masthead" style="background: linear-gradient(0deg,rgba(0, 0, 0, 0),rgba(78, 73, 76, 0)), url(<?php echo esc_url( $full_width_banner['background_image']); ?>);
    background-size: cover;
    background-position: 36%;
    background-color: ">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-6 my-auto">
          <div class="header-content mx-auto">
            <h1 class="content-head" style="color: #fff; font-weight: bold;font-size: 2.9vw;"><?php echo $full_width_banner['heading']; ?></h1>
            <p class="content-head mt-2 mb-4" style="color: #fff; font-weight: 300;font-size:20px;"><?php echo $full_width_banner['description']; ?></p>
            <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger"><?php echo $full_width_banner['button_text']; ?></a>
          </div>
        </div>
        <div class="col-lg-6 my-auto">
          <div class="device-container">
            <div class="device-mockup iphone6_plus portrait white">
              <div class="device">
                <div class="screen">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="<?php echo esc_url( $full_width_banner['image']); ?>" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
 <?php endif; ?>