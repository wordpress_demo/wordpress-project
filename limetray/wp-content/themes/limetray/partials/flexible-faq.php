
<section class="lime-accrodion" style="padding: 60px 0; background-color: <?php the_sub_field('background_color'); ?>;">
		<div class="container">
			<div class="sec-head">
				<h2 style="font-size: 26px; font-weight: 600;font-family: 'Source Sans Pro', sans-serif; color: #222;">More on Restaurant Ordering System</h2>
			</div>
			<div class="row">							
	<?php if( have_rows('accordion') ): ?>
     
	<div class="col-sm-12">

	<?php while( have_rows('accordion') ): the_row(); 

		// vars
		$heading = get_sub_field('heading');
		$content = get_sub_field('content');
		

		?>

		<div id="accordion">

					  <div class="card">
						    <div class="card-header colr">
							      <a class="card-link" data-toggle="collapse" href="#collapseOne">
							         <?php echo $heading; ?>

							      </a>
						    </div>
						    <div id="collapseOne" class="collapse" data-parent="#accordion">
						      <div class="card-body">
							        <?php echo $content; ?>   
					            
						      </div>
						    </div>
					  </div>

			
		   

		</div>

	<?php endwhile; ?>

	</div>

<?php endif; ?>


</div>
</div>
</section>