<section class="features-sec" style="background-color: <?php the_sub_field('background_color'); ?>">
	 <div class="container">

			<div class="sec-head">
				<h2>Product Features</h2>
			</div>
			 <div class="row">
	<?php
	// Name of Repeater field
	$names_array = get_sub_field( 'restaurant_product_features' );
	
	// Counts Users
	$name_count = count( $names_array );
	
	// Number of columns total you want
	$colcounter = 3;
	
	// Determines the number of users per column
	$names_per_list = $name_count / 3 ;
	
	// Rounds decimals up to next number
	$list_names = ceil( $names_per_list );
	
	//splits Array into 3 smaller arrays
	$name_chunks = array_chunk( $names_array, $list_names );
?>       
       
      <?php foreach ( $name_chunks as $name_chunk ) : ?>
      
<?php foreach ( $name_chunk as $names ) : ?>
	 <div class="col-sm-4">
           <div class="item-wrap">
			<?php if($names['image'] ): ?>
				<img src="<?php echo esc_html ($names['image']); ?>"/>
			<?php endif; ?>
         <div class="content-wrap">
				<h3> <?php echo esc_html( $names['heading'] );  ?> </h3>

		      <p>  <?php echo esc_html( $names['content'] ); ?></p>
		 </div>
	     </div>
	      </div>
<?php endforeach; ?>       
<?php endforeach; ?>
    </div>
</div>
</section>
	