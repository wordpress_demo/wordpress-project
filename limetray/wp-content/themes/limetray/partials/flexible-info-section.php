<?php if( have_rows('info_content') ): ?>
    <?php while( have_rows('info_content') ): the_row(); 

        // Get sub field values.
       
        $heading = get_sub_field('heading');
        $text = get_sub_field('text');
         $button_text = get_sub_field('button_text');
       $background_color = get_sub_field('background_color');
        ?>
<section class="info-sec1 sec2 info-new" style="<?php echo $background_color; ?>">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">


        <div class="content-wrap">
        	 <div class="content">
                <h2 style="font-weight:bold; font-size:26px;
      "><?php echo $heading; ?></h2>                          
                <p style="position: relative;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 2vh 0; font-size: 18px;"><?php echo $text; ?></p>
                        
                
                          
             </div>
        </div>


				</div>
			</div>
		</div>
</section>
<?php endwhile; ?>
<?php endif; ?>