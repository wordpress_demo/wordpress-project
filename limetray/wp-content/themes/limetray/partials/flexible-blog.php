
<!-- Section -7 - Start Contact Form Section -->
<section class="form-sec" style="background-color: #f7f9f9;">
		<div class="container">
			<div class="sec-head">
				<h2>Get <span>On-Lime!</span></h2>
				<p class="form">Request a free demonstration to see how our products 
				can help you boost your business</p>
			</div>
			<div class="row">
				<div class="col-sm-12">
               <?php echo do_shortcode("[contact-form-7 id='12658' title='Contact Form']"); ?>					
				</div>
			</div>
		</div>
</section>


<section class="blog-sec page-section" style="background-color: <?php the_sub_field('background_color'); ?>; padding:60px 0;">
		<div class="container">
			<div class="sec-head">
				<h2 style="font-size: 26px; font-weight: 600;font-family: 'Source Sans Pro', sans-serif; color: #222;">From LimeTray's Restaurant Management Blog</h2>
			</div>
			<div class="row">
<?php
	// Name of Repeater field
	$names_array = get_sub_field('blog');
	
	// Counts Users
	$name_count = count( $names_array );
	
	// Number of columns total you want
	$colcounter = 3;
	
	// Determines the number of users per column
	$names_per_list = $name_count / 3 ;
	
	// Rounds decimals up to next number
	$list_names = ceil( $names_per_list );
	
	//splits Array into 3 smaller arrays
	$name_chunks = array_chunk( $names_array, $list_names );
?>       
       
      <?php foreach ( $name_chunks as $name_chunk ) : ?>
      
<?php foreach ( $name_chunk as $names ) : ?>
				<div class="col-sm-4">
					<div class="item-wrap">
						<div class="pic-wrap">
							<?php if($names['image'] ): ?>
				<img class="img-fluid" src="<?php echo esc_html ($names['image']); ?>"/>
			<?php endif; ?>
						</div>
						<div class="content-wrap">
							<h3><?php echo esc_html( $names['heading'] );  ?></h3>
							<p><?php echo esc_html( $names['content'] );  ?></p>
						</div>
						<a href="<?php if($names['link'] ): ?>
        <?php echo esc_html ($names['link']); ?>
      <?php endif; ?>" class="btn"><?php echo esc_html( $names['text'] );  ?></a>
					</div>
				</div>
<?php endforeach; ?>       
<?php endforeach; ?>
			</div>
		</div>
</section>