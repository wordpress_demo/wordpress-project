<div id="our-services" class="our-services">
        <h3 class="text-green mb-5 h1 text-center" style="font-weight: 600;font-size: 2.361111111111111vw;
    line-height: 1.2 !important;font-family: 'Source Sans Pro', sans-serif;color: #222;">The LimeTray Advantage</h3>
        <!-- Features -->
        <div class="features  mb-4">
          <div class="container">
            <div class="row">
              <?php
  // Name of Repeater field
  $names_array = get_sub_field( 'home_advantage' );
  
  // Counts Users
  $name_count = count( $names_array );
  
  // Number of columns total you want
  $colcounter = 2;
  
  // Determines the number of users per column
  $names_per_list = $name_count / 2 ;
  
  // Rounds decimals up to next number
  $list_names = ceil( $names_per_list );
  
  //splits Array into 3 smaller arrays
  $name_chunks = array_chunk( $names_array, $list_names );
?>       
       
      <?php foreach ( $name_chunks as $name_chunk ) : ?>
      
<?php foreach ( $name_chunk as $names ) : ?>

              <div class="feature py-4 col-md-6 d-flex">
                <div class="text-green mr-3"><?php if($names['image'] ): ?>
        <img src="<?php echo esc_html ($names['image']); ?>"/>
      <?php endif; ?></div>
                <div class="px-4">
                    <p style="font-size: 1.6666666666666665vw;
    line-height: 1 !important;font-family: 'Source Sans Pro', sans-serif;color: #222;font-weight: 600;"><?php echo esc_html( $names['heading'] );  ?></p>
                    <p style=""><?php echo esc_html( $names['content'] );  ?></p>
                    <p ><a href="<?php if($names['link'] ): ?>
        <?php echo esc_html ($names['link']); ?>
      <?php endif; ?>"><span style="color: #04b261; font-weight:
      600; text-decoration: underline;"><?php echo esc_html( $names['text'] );  ?></span></a></p>
                </div>
              </div>   
<?php endforeach; ?>       
<?php endforeach; ?>
            </div>
          </div>
        </div>       
      </div>




      <!-- Section -7 - Start Contact Form Section -->
<section class="form-sec" style="background-color: #f7f9f9;">
    <div class="container">
      <div class="sec-head">
        <h2>Get <span>On-Lime!</span></h2>
        <p class="form">Request a free demonstration to see how our products 
        can help you boost your business</p>
      </div>
      <div class="row">
        <div class="col-sm-12">
               <?php echo do_shortcode("[contact-form-7 id='12658' title='Contact Form']"); ?>          
        </div>
      </div>
    </div>
</section>