<!-- Start Template section -->
<section class="temp-sec" style="background-color: #f7f9f9;">
		<div class="container">
			<div class="sec-head">
					<h2 style="font-weight: 600;font-size: 2.0833333333333335vw;
    line-height: 1 !important;">Choose From An Array Of Beautiful Templates</h2>
				</div>
			<div class="row">
	<?php
	// Name of Repeater field
	$names_array = get_sub_field( 'templates' );
	
	// Counts Users
	$name_count = count( $names_array );
	
	// Number of columns total you want
	$colcounter = 3;
	
	// Determines the number of users per column
	$names_per_list = $name_count / 3 ;
	
	// Rounds decimals up to next number
	$list_names = ceil( $names_per_list );
	
	//splits Array into 3 smaller arrays
	$name_chunks = array_chunk( $names_array, $list_names );
?>       
       
      <?php foreach ( $name_chunks as $name_chunk ) : ?>
      
<?php foreach ( $name_chunk as $names ) : ?>
	 <div class="col-md-4">
					<div class="item-wrap">
						<img src="<?php bloginfo('template_url'); ?>/assets/img/limetray_product_ace_laptop.svg" class="img-fluid">
						<div class="wrap">
							<div class="pic-wrap">
								
							</div>
							<a href="<?php if($names['link'] ): ?>
        <?php echo esc_html ($names['link']); ?>
      <?php endif; ?>" class="pic-overlay">
								<span>Visit Website</span>
							</a>
						</div>

						<h5><?php echo esc_html( $names['text'] );  ?></h5>
					</div>
				</div>
<?php endforeach; ?>       
<?php endforeach; ?>
    </div>
</div>
</section>
