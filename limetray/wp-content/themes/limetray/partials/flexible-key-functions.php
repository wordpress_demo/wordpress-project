 <div id="third-section-a" class="page-section" style="padding: 60px 0;">
<div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center" style="padding-bottom: 50px">
                          <h2 class="font_family_a font_size_36px font_color_b font-600 wow animate fadeInUp intro-text-2" style="font-size: 26px;">Easy integrations with our partners</h2>
                      </div>
                  </div>

                     <div class="row" >
                            

                                <div class="owl-carousel wow animate fadeInUpImg" id="owl-clients">
<?php
 $images = get_sub_field('restaurant');
 foreach ($images as $image):?>                  <div class="item">
                                       <img src="<?php echo $image['sizes']['medium']; ?>" class="img-fluid" style="width: 181px; height: 44px" />
                                    </div>
<?php endforeach;?>
                                </div>
                            </div>
                            <script>
                                $("#third-section-a .owl-carousel").owlCarousel({
                                    autoPlay: 2600,
                                    navigation: true,
                                    center: true,
                                    loop: true,
                                    pagination: false,
                                    slideSpeed: 700,
                                    items: 7,
                                    itemsDesktop: [1400, 6],
                                    itemsDesktopSmall: [1100, 5],
                                    itemsTablet: [700, 5],
                                    itemsMobile: [500, 2]
                                });

                                $(".next").click(function() {
                                    owl.trigger('owl.next');
                                })
                                $(".prev").click(function() {
                                    owl.trigger('owl.prev');
                                })
                                $(".play").click(function() {
                                    owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
                                })
                                $(".stop").click(function() {
                                    owl.trigger('owl.stop');
                                });
                            </script>
                        </div>
                    </div>

  <!-- End Rating Section -->

  <!-- Section -7 - Start Contact Form Section -->
<section class="form-sec" style="background-color: #f7f9f9;">
        <div class="container">
            <div class="sec-head">
                <h2>Get <span>On-Lime!</span></h2>
                <p class="form">Request a free demonstration to see how our products 
                can help you boost your business</p>
            </div>
            <div class="row">
                <div class="col-sm-12">
               <?php echo do_shortcode("[contact-form-7 id='12658' title='Contact Form']"); ?>                  
                </div>
            </div>
        </div>
</section>
