

<!-- Start Laptop Tab section -->

	<section class="lap-tab-sec hover-video" id="vid1-play" style="background-color: #fff;">
		<div class="container">
			<?php if( have_rows('laptop_animation') ): ?>
    <?php while( have_rows('laptop_animation') ): the_row(); 

        // Get sub field values.
       
        ?> 
			<div class="row">
				<div class="col-md-5 float-right">
					<ul class="tab">
					 	<li class="active">
					 		 <a href="#lap1">
							   <h3> <?php the_sub_field('heading'); ?></h3>
							   <p> <?php the_sub_field('text'); ?></p>
							   <div class="progress-bar"></div>
							 </a>
					 	</li>
					 	<li>
					 		 <a href="#lap2">
							  	<h3> <?php the_sub_field('heading_1'); ?></h3>
							   	<p> <?php the_sub_field('text-1'); ?></p>
							   	<div class="progress-bar"></div>
							  </a>
					 	</li>
					 
					 	<li>
					 		 <a href="#lap3">
							  	<h3> <?php the_sub_field('heading_2'); ?></h3>
							   	<p> <?php the_sub_field('text-2'); ?></p>
							   	<div class="progress-bar"></div>
							  </a>
					 	</li>
					  	
					  	<li>
					  		 <a href="#lap4">
							  	<h3> <?php the_sub_field('heading_3'); ?></h3>
							   	<p> <?php the_sub_field('text-3'); ?></p>
							   	<div class="progress-bar"></div>
							  </a>
					  	</li>
					</ul>
				</div>
				<div class="col-md-7">
					<div class="pic-wrap">
						<div class="vid-bg ">
							<img src="<?php bloginfo('template_url'); ?>/assets/img/pd-tabs-bg.jpg" class="img-fluid">
						</div>
						<div class="tab-content">
							<div id="lap1" class="tabcontent active">
							 	<div class="pic-wrap1">
							 		<img src="<?php bloginfo('template_url'); ?>/assets/img/gif/order-online-gif1.gif">
							 	</div>
							</div>

							<div id="lap2" class="tabcontent">
								<div class="pic-wrap1">
							 		<img src="<?php bloginfo('template_url'); ?>/assets/img/gif/order-online-gif2.gif">
							 	</div>
							</div>

							<div id="lap3" class="tabcontent">
							 	<div class="pic-wrap1">
							 		<img src="<?php bloginfo('template_url'); ?>/assets/img/gif/order-online-gif3.gif">
							 	</div>
							</div>

							<div id="lap4" class="tabcontent">
							 	<div class="pic-wrap1">
							 		<img src="<?php bloginfo('template_url'); ?>/assets/img/gif/order-online-gif4.gif">
							 	</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<?php endwhile;?>
            <?php endif; ?>
		</div>
	</section> 

	<!-- End Laptop Tab section -->




	<!-- End Laptop Tab section -->